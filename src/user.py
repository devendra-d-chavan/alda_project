#!/usr/bin/python

"""
Contains the user class

Author: Devendra D. Chavan [@ddchavan]
"""

class User(object):
    """
    Represents the user
    """
    def __init__(self, str_):
        """
        Initializes the user after tokenising the specified string
        """
        id_, gender, age, _, _ = str_.split('::')
        self.id_ = id_
        self.age = age
        self.gender = gender

    def serialize(self):
        """
        Returns a string containing the user's data members 
        """
        return '{0}\t{1}\t{2}'.format(self.id_, self.age, self.gender)
