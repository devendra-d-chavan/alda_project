#!/usr/bin/python

"""
Contains the movie class

Author: Devendra D. Chavan [@ddchavan]
"""

class Movie(object):
    """
    Represents the movie object
    """

    def __init__(self, str_):
        """
        Initializes the movie object by tokenising the specified string
        """
        right_index = str_.rfind('::')
        left_index = str_.find('::')

        if right_index != -1 and left_index != -1:
            self.id_ = str_[:left_index]
            movie_name_year = str_[left_index + len('::') : right_index]
            self.title = movie_name_year[:movie_name_year.rfind('(')].strip()
            self.year = movie_name_year[movie_name_year.rfind('(') + 1 : 
                                    movie_name_year.rfind(')')]
            self.genres = [genre.strip() 
                        for genre in str_[right_index + len('::'):].split('|')]

        self.rating = 0
        self.imdb_id = ''
        self.actors = None
        self.release_date = ''
        self.directors = ''

    def query_imdb(self, use_local_database = False):
        """
        Queries the IMDB API to retreive the movie meta data
        Populates the data members of this class.
            Contains the following keys:
            * rating
            * genre (list)
            * title 
            * release_date (in yyyymmdd format)
            * actors (list)
            * directors (list)
            * imdb_id 

        Args:
            movie_name: Name of the movie to query
            year:       Release date of the movie

        Returns:
            Nothing
        """
        if use_local_database:
            content = self._query_local_imdb_dataset()
        else:
            content = self._query_remote_imdb_dataset()
           
        if content is None:
            return

        if 'rating' in content:
            self.rating = content['rating']
        
        if 'genres' in content:
            self.genres = [self._unescape(genre).strip()
                            for genre in content['genres']]

        #if 'title' in content:
            #self.title = self._unescape(content['title'])

        if 'imdb_id' in content:
            self.imdb_id = content['imdb_id']

        if 'actors' in content:
            self.actors = [self._unescape(actor).strip()
                            for actor in content['actors']]

        if 'release_date' in content:
            self.release_date = content['release_date']

        if 'directors' in content:
            self.directors = [self._unescape(director).strip()
                                for director in content['directors']]
        
    def serialize(self):
        """
        Returns a string representing the movie's data members
        """
        max_genres_len = 10
        genres_list = ''

        for genre in self.genres:
            genres_list += genre + '\t'

        genres_list = genres_list.strip()

        for i in xrange(len(self.genres), max_genres_len):
            genres_list += '\t'

        return '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}' \
                .format(self.id_, self.title, self.release_date, self.rating,
                        genres_list, self.actors, self.directors)
        
    def _query_remote_imdb_dataset(self):
        from httplib2 import Http
        import simplejson as json
        import urllib

        params = urllib.urlencode({'title' : self.title, 'type' : 'json', 
                                    'plot' : 'simple', 'episode' : 0, 
                                    'limit' : 1, 'yg' : self.year, 
                                    'mt' : 'none', 'lang' : 'en-US', 
                                    'offset' : '', 'aka' : 'simple', 
                                    'release' : 'simple', 'business' : 0, 
                                    'tech' : 0})

        h = Http()
        resp, content = h.request('http://mymovieapi.com/?' + params, 'GET')

        if resp['status'] == '200':
            content = json.loads(content)
 
            if type(content) == list and len(content) > 0:
                content = content[0]

                with open('movie_dataset', 'a') as f:
                    f.write(self.id_ + '\t' + str(content) + '\n')

                return content

            return None

    def _query_local_imdb_dataset(self):
        import ast
        with open('movie_dataset') as f:
            for line in f: 
                line = line.strip()
                index = line.find('\t')
                if self.id_ == line[:index]:
                    return ast.literal_eval(line[index+1:])

        return None

    def _unescape(self, str_):
        """
        Replaces HTML escape sequences with regular strings

        Args:
            str_: String to unescape

        Returns:
            The unescaped string
        """
        import HTMLParser

        return str(HTMLParser.HTMLParser().unescape(str_.encode('utf-8'))) 
