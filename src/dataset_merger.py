#!/usr/bin/python

"""
Script to merge the movie lens datasets into a flat file and query the IMDB
API for additional information (IMDB rating, genre, cast, release date and 
imdb id (not required)

Schema:
The movie lens dataset directory contains the following files
 * movielens/users.dat: UserID::Gender::Age::Occupation::Zip-code
 * movielens/movies.dat: MovieID::Title::Genres
 * movielens/rating.dat: UserID::MovieID::Rating::Timestamp

Query using mymovieapi.com:
http://mymovieapi.com/?title=Cats+and+Dogs&type=json&plot=simple&episode=0&limit=1&year=2001&yg=1&mt=M&lang=en-US&offset=&aka=simple&release=simple&business=0&tech=0

Usage:
python dataset_merger.py <movie lens dataset directory> <output filepath> <use local dataset = TRUE/FALSE>

Example:
python dataset_merger.py ../dataset/movielens merged.dat TRUE

Author: Devendra D. Chavan [@ddchavan]
"""
class DatasetMerger:
    """
    Contains logic to merge the users, ratings and movie dataset, and include
    movie meta data queried from IMDB API
    """

    def __init__(self):
        """
        Initializes the current instance
        """
        # Nothing to do
        pass

    def merge_datasets(self, dataset_dirpath, output_filepath, 
                        use_local_dataset):
        """
        Merges the movies, ratings and users datasets present in the specified
        directory to the output file.

        Sleeps for 2.5 seconds between IMDB query requests.

        Args:
            dataset_dirpath:    Path to the directory containing the datasets
                                (movies.dat, ratings.dat, users.dat)
            output_filepath:    Merged output file path 

        Returns:
            None
        """
        import os
        import time
        from movie import Movie
        from user import User

        users_filepath = os.path.join(dataset_dirpath, 'users.dat')
        movies_filepath = os.path.join(dataset_dirpath, 'movies.dat')
        ratings_filepath = os.path.join(dataset_dirpath, 'ratings.dat')

        # Load the list of users
        users = dict()
        with open(users_filepath) as users_file:
            for line in users_file:
                user = User(line)
                users[user.id_] = user

        # Load the list of movies
        movies = dict()
        counter = 1
        with open(movies_filepath) as movies_file:
            for line in movies_file:
                movie = Movie(line)

                # API limits to 30 requests per minutes i.e. 1 requst every 2
                # seconds and upto 2500 per day
                # => Sleep for 2.5 seconds between requests for 2400 requests
                # Expected run time = 2400 movies * 2.5 sec ~ 1 hour 40 minutes
                print 'Querying {0}: movie {1}'.format(counter, movie.title)
                movie.query_imdb(use_local_dataset)

                if not use_local_dataset:
                    time.sleep(2.5)

                movies[movie.id_] = movie
                counter += 1

        # Flatten ratings dataset
        with open(ratings_filepath) as ratings_file, \
            open(output_filepath, 'w') as output_file:
            for line in ratings_file:
                user_id, movie_id, rating = self._tokenise_rating(line)            

                if user_id in users and movie_id in movies:
                    user = users[user_id]
                    movie = movies[movie_id]                                
                
                    output_file.write('{0}\t{1}\t{2}\n'.format(movie.serialize(), 
                                                                user.serialize(),
                                                                rating))                    
    def _tokenise_rating(self, str_):
        """ 
        Tokenises the specified string into rating

        Returns:
            A tuple contains the user_id, movie_id and rating
        """
        user_id, movie_id, rating, _ = str_.split('::')
        return (user_id, movie_id, rating)

if __name__ == '__main__':  
    import sys

    dataset_path = sys.argv[1]
    output_filepath = sys.argv[2]
    use_local_dataset = bool(sys.argv[3])

    dataset_merger = DatasetMerger()
    dataset_merger.merge_datasets(dataset_path, output_filepath, 
                                    use_local_dataset)
