This report is created using LaTeX. If you are new to it, here are some steps 
to get you started (Ubuntu).

Pre requisites
-----------------

  1. LaTeX installation: `sudo apt-get install texlive texlive-doc-en`

Compiling the bibliography
-----------------
This needs to done only for the first time or when there is a change in the 
list of papers (`papers.bib`). The papers are specified in the BibTex format.

    > bibtex project

Compiling the project report
----------------
The report is divided into sub sections (like abstract, intro, etc.) each 
within its own separate file. They are integrated into the main project report
file `project.tex`

To compile the report, `pdflatex project` (run twice if there are any change in
the references). 

Citing references
----------------
References can be cited by using the `\cite{<short name>}` where `<short name>` 
refers to the name of the paper in the `papers.bib` file (this file is 
created by exporting each paper's reference in BibTex format from google 
scholar).
