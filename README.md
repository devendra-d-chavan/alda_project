CSC 522: Automated Learning and Data Analysis - Term project Fall 2013
==========================

Personalized Movie Recommendation System
-------------------------

Team
-------------------------

 * Anudeep Sadashivappa [asadash at ncsu dot edu]
 * Devendra D. Chavan [ddchavan at ncsu dot edu]
 * Karthika Muthiah [kmuthia at ncsu dot edu]
 * Smitha Sathyanarayana [ssathya at ncsu dot edu]
 * Sunil Kalmadka [skalmad at ncsu dot edu]

Overview
-----------------------
This project evaluates the accuracy of a hybrid approach with **UBCF & IBCF
collaborative filtering** and **content based filtering** for a **movie 
recommendation system** by utilizing user demographics. Here content refers to 
the attributes of the movies such as genre, release date, critic rating, cast, 
director, etc.

Dataset merge script
-------------------
Usage
===================
`python dataset_merger.py <dataset directory path> <merged filepath> <read from local dataset>`

Example
==================
Read from local dataset - movie_dataset (present in the current directory)
`python dataset_merger.py ../datasets/movielens merged.dat TRUE`

Read from remote *mymovieapi.com*
`python dataset_merger.py ../datasets/movielens merged.dat FALSE`

IMDB API limitations
==================
The [mymovieapi.com][1] API restricts the REST calls at 30 per minutes and 2500
per day (by IP address?). The workaround is to split the `movies.dat` dataset
into less than `2500` items (for example, `2400` items) and use the above 
script that requests the service every `2.5` seconds. The output files can then
be merged together.

Merged dataset format
=================
Each row (new line delimited) contains the following attributes (tab delimited)

 1. movie.id
 2. movie.title
 3. movie.release_date (in yyyymmdd format)
 4. movie.rating    (imdb rating)
 5. movie.genres    (tab delimited list - 10 genres)
 6. movie.actors    (comma delimited list, enclosed within a pair of square braces `[ ]`)
 7. movie.directors (comma delimited list, enclosed within a pair of square braces `[ ]`)
 8. user.id
 9. user.age
 10. user.gender
 11. movielens.rating

Generating the models/predictions
-----------------
The `R` script the generate the recommendations is `src/main.R`. Depending on
the host `R` installation, uncomment the lines to install the required `R` 
packages before running. Note that creating of the `real_rating_matrix` can 
take a long time (~30 minutes on 2.4GHz Intel i5 - 8 GB RAM laptop). The code
itself is self explanatory and comments have been added where relevant.

Poster and report
----------------
The project poster and report pdfs are present in the `docs` directory.

 [1]: http://mymovieapi.com/
